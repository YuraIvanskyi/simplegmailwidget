# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtGui, QtWidgets
import smtplib
import re

class Ui_Form(object):
    def setupUi(self, Form):
        """This function is generated by pyuic5 from pre-made ui in designer"""
        Form.setObjectName("Form")
        Form.resize(500, 250)
        Form.setMinimumSize(QtCore.QSize(500, 250))
        Form.setMaximumSize(QtCore.QSize(500, 250))
        self.stackedWidget = QtWidgets.QStackedWidget(Form)
        self.stackedWidget.setGeometry(QtCore.QRect(10, 0, 481, 251))
        self.stackedWidget.setObjectName("stackedWidget")
        self.Logpage = QtWidgets.QWidget()
        self.Logpage.setObjectName("Logpage")
        self.formLayoutWidget = QtWidgets.QWidget(self.Logpage)
        self.formLayoutWidget.setGeometry(QtCore.QRect(120, 30, 241, 71))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(10, 10, 10, -1)
        self.formLayout.setSpacing(10)
        self.formLayout.setObjectName("formLayout")
        self.loginLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.loginLabel.setObjectName("loginLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.loginLabel)
        self.loginLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.loginLineEdit.setObjectName("loginLineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.loginLineEdit)
        self.passwordLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.passwordLabel.setObjectName("passwordLabel")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.passwordLabel)
        self.passwordLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.passwordLineEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordLineEdit.setObjectName("passwordLineEdit")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.passwordLineEdit)
        self.login_Button = QtWidgets.QPushButton(self.Logpage)
        self.login_Button.setGeometry(QtCore.QRect(190, 120, 100, 24))
        self.login_Button.setFlat(False)
        self.login_Button.setObjectName("login_Button")
        self.system_state_label = QtWidgets.QLabel(self.Logpage)
        self.system_state_label.setGeometry(QtCore.QRect(200, 170, 81, 16))
        self.system_state_label.setScaledContents(False)
        self.system_state_label.setWordWrap(False)
        self.system_state_label.setObjectName("system_state_label")
        self.stackedWidget.addWidget(self.Logpage)
        self.Messagepage = QtWidgets.QWidget()
        self.Messagepage.setObjectName("Messagepage")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.Messagepage)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(0, 5, 481, 241))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.send_layout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.send_layout.setContentsMargins(10, 10, 10, 10)
        self.send_layout.setObjectName("send_layout")
        self.receivers_layout = QtWidgets.QVBoxLayout()
        self.receivers_layout.setObjectName("receivers_layout")
        self.new_receiver_edit = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.new_receiver_edit.setObjectName("new_receiver_edit")
        self.receivers_layout.addWidget(self.new_receiver_edit)
        self.add_receiver_btn = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.add_receiver_btn.setObjectName("add_receiver_btn")
        self.receivers_layout.addWidget(self.add_receiver_btn)
        self.receivers_list_widget = QtWidgets.QListWidget(self.horizontalLayoutWidget)
        self.receivers_list_widget.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.receivers_list_widget.sizePolicy().hasHeightForWidth())
        self.receivers_list_widget.setSizePolicy(sizePolicy)
        self.receivers_list_widget.setEditTriggers(QtWidgets.QAbstractItemView.DoubleClicked|QtWidgets.QAbstractItemView.EditKeyPressed|QtWidgets.QAbstractItemView.SelectedClicked)
        self.receivers_list_widget.setAlternatingRowColors(True)
        self.receivers_list_widget.setResizeMode(QtWidgets.QListView.Fixed)
        self.receivers_list_widget.setObjectName("receivers_list_widget")
        self.receivers_layout.addWidget(self.receivers_list_widget)
        self.send_layout.addLayout(self.receivers_layout)
        self.message_layout = QtWidgets.QVBoxLayout()
        self.message_layout.setContentsMargins(0, 0, 0, 0)
        self.message_layout.setSpacing(10)
        self.message_layout.setObjectName("message_layout")
        self.subject_Edit = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.subject_Edit.setText("")
        self.subject_Edit.setObjectName("subject_Edit")
        self.message_layout.addWidget(self.subject_Edit)
        self.message_Edit = QtWidgets.QTextEdit(self.horizontalLayoutWidget)
        self.message_Edit.setObjectName("message_Edit")
        self.message_layout.addWidget(self.message_Edit)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.sendButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.sendButton.setObjectName("sendButton")
        self.horizontalLayout.addWidget(self.sendButton)
        self.cancelButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.message_layout.addLayout(self.horizontalLayout)
        self.send_layout.addLayout(self.message_layout)
        self.stackedWidget.addWidget(self.Messagepage)
        self.Systempage = QtWidgets.QWidget()
        self.Systempage.setObjectName("Systempage")
        self.label = QtWidgets.QLabel(self.Systempage)
        self.label.setGeometry(QtCore.QRect(190, 30, 100, 24))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.Systempage)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(160, 100, 160, 83))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.afterwork_menu_layout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.afterwork_menu_layout.setObjectName("afterwork_menu_layout")
        self.new_message_btn = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.new_message_btn.setObjectName("new_message_btn")
        self.afterwork_menu_layout.addWidget(self.new_message_btn)
        self.logout_btn = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.logout_btn.setObjectName("logout_btn")
        self.afterwork_menu_layout.addWidget(self.logout_btn)
        self.exit_btn = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.exit_btn.setObjectName("exit_btn")
        self.afterwork_menu_layout.addWidget(self.exit_btn)
        self.stackedWidget.addWidget(self.Systempage)

        # actually my added line:)
        self.ConnectEvents()

        self.retranslateUi(Form)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Simple Gmail 1.0"))
        self.loginLabel.setText(_translate("Form", "Login"))
        self.loginLineEdit.setPlaceholderText(_translate("Form", "email@gmail.com"))
        self.passwordLabel.setText(_translate("Form", "Password"))
        self.passwordLineEdit.setPlaceholderText(_translate("Form", "Password"))
        self.login_Button.setText(_translate("Form", "Log in"))
        self.system_state_label.setText(_translate("Form", ""))#here is result!!!<----
        self.new_receiver_edit.setPlaceholderText(_translate("Form", "email@mail.com"))
        self.add_receiver_btn.setText(_translate("Form", "Add"))
        self.receivers_list_widget.setSortingEnabled(True)
        __sortingEnabled = self.receivers_list_widget.isSortingEnabled()
        self.receivers_list_widget.setSortingEnabled(False)
        self.receivers_list_widget.setSortingEnabled(__sortingEnabled)
        self.subject_Edit.setPlaceholderText(_translate("Form", "Subject:"))
        self.message_Edit.setPlaceholderText(_translate("Form", "Message:"))
        self.sendButton.setText(_translate("Form", "Send"))
        self.cancelButton.setText(_translate("Form", "Cancel"))
        self.label.setText(_translate("Form", "Success!"))
        self.new_message_btn.setText(_translate("Form", "New message"))
        self.logout_btn.setText(_translate("Form", "Log out"))
        self.exit_btn.setText(_translate("Form", "Exit"))

##################################LOGIC####################################

    def Login(self,login,password):
        """Creating a connection to server by login and pass on port 587"""
        self.connection = smtplib.SMTP('smtp.gmail.com', 587)
        self.connection.starttls()
        try:
            self.connection.login(login, password)
            self.PageChange(1)
        except:
            self.system_state_label.setText("Auth failed!")

    def Messaging(self,login,message,receivers):
        """sending a message via an existing connection"""
        self.connection.sendmail(login,receivers,message)

    def CloseConnection(self):
        """Closing an existing connection"""
        self.connection.quit()

#############################Data Extraction#############################

    def Get_login(self):
        """fetch login adress"""
        return self.loginLineEdit.text()

    def Get_password(self):
        """fetch password"""
        return self.passwordLineEdit.text()

    def ComposeMessage(self):
        """fetch a plaintext message from the controls - textholders"""
        return self.subject_Edit.text()+'\n'+self.message_Edit.toPlainText()

    def ExtractReceivers(self):
        """fetch a list of receivers from listwidget"""
        receivers = [str(self.receivers_list_widget.item(i).text()) for i in range(self.receivers_list_widget.count())]
        return receivers

###########################LOGIC/UI Interaction##########################

    def PageChange(self, index):
        """Jumping between pages of widget"""
        self.stackedWidget.setCurrentIndex(index)

    def Login_btn_click(self):
        """btn click event to login via form"""
        self.Login(self.Get_login(),self.Get_password())


    def Add_receiver_click(self):
        """btn click event to add new receiver to list widget"""
        if re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)",self.new_receiver_edit.text()):
            receiver = self.new_receiver_edit.text()
            item = QtWidgets.QListWidgetItem(receiver)
            self.receivers_list_widget.addItem(item)
        else:
            self.new_receiver_edit.setPlaceholderText("Not correct. Write: email@mail.com")

    def Send_message_click(self):
        """btn click event to send the message"""
        message = self.ComposeMessage()
        receivers = self.ExtractReceivers()
        sender = self.Get_login()
        self.Messaging(sender,message,receivers)
        self.PageChange(2)

    def new_message_btn_click(self):
        """redirects to messaging page, clears fields"""
        self.PageChange(1)
        self.receivers_list_widget.clear()
        self.message_Edit.clear()
        self.subject_Edit.clear()

    # def list_item_dblclick(self):
    #     """deletes a receiver while dblclicking on it"""
    #     #TODO

    def logout_btn_click(self):
        """closes connection, returns to login page"""
        self.CloseConnection()
        self.loginLineEdit.clear()
        self.passwordLineEdit.clear()
        self.PageChange(0)

    def exit_btn_click(self):
        """closes connection, quits from app"""
        self.CloseConnection()
        self.close()

    def cancel_btn_click(self):
        """closes connection, returns to login"""
        self.CloseConnection()
        self.PageChange(0)

    def ConnectEvents(self):
        """connects functions with their logical events"""
        self.login_Button.clicked.connect(self.Login_btn_click)
        self.add_receiver_btn.clicked.connect(self.Add_receiver_click)
        self.sendButton.clicked.connect(self.Send_message_click)
        self.new_message_btn.clicked.connect(self.new_message_btn_click)
        self.logout_btn.clicked.connect(self.logout_btn_click)
        self.exit_btn.clicked.connect(self.exit_btn_click)
        self.cancelButton.clicked.connect(self.cancel_btn_click)
        #TODO Add double click for item in listwidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    # Form.setWindowFlags(Form.windowFlags() | QtCore.Qt.FramelessWindowHint)
    # Form.setAttribute(QtCore.Qt.WA_TranslucentBackground)
    #TODO this plays nice but not the best background:)
    Form.show()
    sys.exit(app.exec_())

